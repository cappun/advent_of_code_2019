//! # Advent Of Code 2019
//! This is my personal attempt at solving all the challenges of AOC 2019
//! for fun and for learning more about Rust 🦀

pub mod day_one;
pub mod day_two;
pub mod day_three;