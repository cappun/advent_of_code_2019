use std::fs::File;
use std::io::{BufReader, Read};
use std::path::Path;
use std::env::current_dir;

pub struct IntcodeCpu {
    program_counter: usize,
    first_operand_address: usize,
    second_operand_address: usize,
    output_address: usize,
}

impl IntcodeCpu{
    pub fn new() -> Self{
        IntcodeCpu{
            program_counter: 0,
            first_operand_address: 0,
            second_operand_address: 0,
            output_address: 0
        }
    }
    pub fn reset(&mut self) {
        self.first_operand_address = 0;
        self.second_operand_address = 0;
        self.program_counter = 0;
        self.output_address = 0;
    }
    pub fn execute_program(&mut self, program: &mut Vec<i32>) {
        while program[self.program_counter] != 99 {
            self.output_address = program[self.program_counter + 3] as usize;
            self.first_operand_address = program[self.program_counter + 1] as usize;
            self.second_operand_address = program[self.program_counter + 2] as usize;
            match program[self.program_counter]{
                1 => {
                    program[self.output_address] = program[self.first_operand_address] + program[self.second_operand_address];
                },
                2 => {
                    program[self.output_address] = program[self.first_operand_address] * program[self.second_operand_address];
                },
                _ => panic!("Unknown opcode")
            }
            self.program_counter += 4;
        }
    }
}

fn parse_program() -> Vec<i32> {
    println!("{:?}", current_dir());
    let mut unparsed = String::new();
    let file = File::open(Path::new("src/day_two/input.txt")).unwrap();
    let mut reader = BufReader::new(file);
    reader.read_to_string(&mut unparsed);
    unparsed
        .split(",")
        .map(|num| num.parse::<i32>().unwrap())
        .collect()
}

#[cfg(test)]
mod tests {
    use crate::day_two::{parse_program, IntcodeCpu};
    use std::io::stdin;

    #[test]
    fn simple_test_one() {
        let mut program = vec![1,1,1,4,99,5,6,0,99];
        let mut cpu = IntcodeCpu::new();
        cpu.execute_program(&mut program);
        assert_eq!(program, vec![30,1,1,4,2,5,6,0,99]);
    }
    #[test]
    fn simple_test_two() {
        let mut program = vec![2,4,4,5,99,0];
        let mut cpu = IntcodeCpu::new();
        cpu.execute_program(&mut program);
        assert_eq!(program, vec![2,4,4,5,99,9801]);
    }

    #[test]
    fn part_one_solution() {
        let mut intcode_cpu = IntcodeCpu::new();
        let mut program = parse_program();
        //Restore "1202 program alarm" state
        program[1] = 12;
        program[2] = 2;
        intcode_cpu.execute_program(&mut program);
        assert_eq!(program[0], 5305097);
    }
    #[test]
    fn part_two_solution() {
        let mut intcode_cpu = IntcodeCpu::new();
        let program = parse_program();
        let mut searched_input: (i32, i32) = (0, 0);
        'outer: for noun in 0..=99 {
            for verb in 0..=99 {
                let mut memory = program.clone();
                memory[1] = noun;
                memory[2] = verb;
                intcode_cpu.execute_program(&mut memory);
                intcode_cpu.reset();
                if(memory[0] == 19690720){
                    searched_input.0 = noun;
                    searched_input.1 = verb;
                    break 'outer;
                }
            }
        }
        let answer = 100 * searched_input.0 + searched_input.1;
        assert_eq!(answer, 4925);
    }
}