use std::io::{BufReader, BufRead};
use std::fs::File;
use std::fmt::{Display, Formatter, Error};
use std::fmt;
use std::collections::HashSet;
use std::collections::hash_set::Intersection;
use std::hash::{Hash, Hasher};

// I'm not very happy about how day three solution is implemented
// I'll move on for the moment and refactor it in the future
// TODO: Refactor me please :)

#[derive(Debug, Clone)]

/// # Point
/// This struct represent a Point on a Cartesian Plane
pub struct Point(i32, i32, i32);

const ORIGIN: Point = Point(0, 0, 0);

impl Point {
    fn manhattan_distance(&self) -> i32 {
        (self.0 - ORIGIN.0).abs() + (self.1 - ORIGIN.1).abs()
    }
}

impl Hash for Point {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.0.hash(state);
        self.1.hash(state);
    }
}

impl PartialEq for Point {
    fn eq(&self, other: &Self) -> bool {
        self.0 == other.0 && self.1 == other.1
    }
}

impl Eq for Point {}

#[derive(Debug)]
pub struct Path {
    points: HashSet<Point>
}

impl Path {
    pub fn from(movements: &Vec<Movement>) -> Self {
        let mut path = Path{
            points: HashSet::new()
        };
        let mut current_point = Point(0, 0, 0);
        path.points.insert(current_point.clone());
        for (index, movement) in movements.iter().enumerate() {
            match movement{
                Movement::Up(distance) => {
                    for i in 0..*distance {
                        current_point.1 += 1;
                        current_point.2 += 1;
                        path.points.insert(current_point.clone());
                    }
                },
                Movement::Down(distance) => {
                    for i in 0..*distance {
                        current_point.1 -= 1;
                        current_point.2 += 1;
                        path.points.insert(current_point.clone());
                    }
                },
                Movement::Left(distance) => {
                    for i in 0..*distance {
                        current_point.0 -= 1;
                        current_point.2 += 1;
                        path.points.insert(current_point.clone());
                    }
                },
                Movement::Right(distance) => {
                    for i in 0..*distance {
                        current_point.0 += 1;
                        current_point.2 += 1;
                        path.points.insert(current_point.clone());
                    }
                },
            }
        }
        path
    }
    pub fn intersect(&self, path: &Path) -> Vec<Point> {
        let mut intersection_points: Vec<Point> = Vec::new();
        for point in self.points.intersection(&path.points){
            if point.0 != 0 && point.1 != 0 {
                intersection_points.push(point.clone())
            }
        }
        intersection_points
    }
}

#[derive(Debug)]
pub enum Movement {
    Up(i32),
    Down(i32),
    Left(i32),
    Right(i32)
}

pub fn load_input(path: impl AsRef<std::path::Path>) -> [Vec<Movement>; 2] {
    let file = File::open(path).unwrap();
    let reader = BufReader::new(file);
    let mut wires_paths: [Vec<Movement>; 2] = [vec![], vec![]];
    for (wire_number, line) in reader.lines().enumerate() {
        for movement in line.unwrap().split(",") {
            let (direction, distance) = movement.split_at(1);
            let distance = distance.parse::<i32>().unwrap();
            match direction {
                "U" => wires_paths[wire_number].push(Movement::Up(distance)),
                "D" => wires_paths[wire_number].push(Movement::Down(distance)),
                "L" => wires_paths[wire_number].push(Movement::Left(distance)),
                "R" => wires_paths[wire_number].push(Movement::Right(distance)),
                _ => panic!("Not a valid direction")
            }
        }
    }
    wires_paths
}

#[cfg(test)]
mod tests {
    use crate::day_three::{load_input, Path, Point};
    use std::collections::HashSet;

    fn solve_manhattan(input: impl AsRef<std::path::Path>) -> i32 {
        let input = load_input(input);
        let first_wire = Path::from(&input[0]);
        let second_wire = Path::from(&input[1]);
        let intersections: Vec<Point> = first_wire.intersect(&second_wire);
        let mut distances = Vec::new();
        for intersection in intersections {
            distances.push(intersection.manhattan_distance())
        }
        distances.into_iter().min().unwrap()
    }

    fn solve_signal_delay(input: impl AsRef<std::path::Path>) -> i32 {
        let input = load_input(input);
        let first_wire = Path::from(&input[0]);
        let second_wire = Path::from(&input[1]);
        let intersections: Vec<Point> = first_wire.intersect(&second_wire);
        let mut distances: Vec<i32> = Vec::new();
        for point in intersections.iter(){
            let first_distance = first_wire.points.get(point).unwrap().2;
            let second_distance = second_wire.points.get(point).unwrap().2;
            distances.push(first_distance + second_distance);
        }
        distances.into_iter().min().unwrap()
    }

    #[test]
    fn simple_test_manhattan(){
        assert_eq!(solve_manhattan("src/day_three/simple_input.txt"), 159);
    }
    #[test]
    fn simple_test_delay(){
        assert_eq!(solve_signal_delay("src/day_three/simple_input.txt"), 610);
    }
    #[test]
    fn solve_part_one(){
        assert_eq!(solve_manhattan("src/day_three/input.txt"), 1674);
    }
    #[test]
    fn solve_part_two(){
        assert_eq!(solve_signal_delay("src/day_three/input.txt"), 14012);
    }
}
