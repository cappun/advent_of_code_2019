use std::ops::{Div, Sub};
use std::fs::File;
use std::io::{Read, BufReader, BufRead};

fn load_input() -> BufReader<File> {
    let file = File::open("src/day_one/input.txt").unwrap();
    BufReader::new(file)
}

pub fn required_fuel(mass: i32) -> i32 {
    let mass = mass as f32;
    (mass.div(3f32).floor() as i32).sub(2)
}

pub fn recursive_required_fuel(mass: i32) -> i32 {
    let required = required_fuel(mass);
    if required <= 0 {
        return 0;
    }else{
        return required + recursive_required_fuel(required);
    }
}

pub fn solve(fuel_fn: fn (i32) -> i32) -> i32 {
    let mut accumulator = 0;
    for mass in load_input().lines() {
        let mass = mass.unwrap().parse::<i32>().unwrap();
        accumulator += fuel_fn(mass);
    }
    accumulator
}

#[cfg(test)]
mod tests {
    use crate::day_one::{required_fuel, solve, recursive_required_fuel};
    #[test]
    fn for_mass_1969_fuel_should_be_654() {
        assert_eq!(required_fuel(1969), 654);
    }
    #[test]
    fn for_mass_1969_recursive_fuel_should_be_966() {
        assert_eq!(recursive_required_fuel(1969), 966);
    }
    #[test]
    fn solution() {
        //Solve Part I
        assert_eq!(solve(required_fuel), 3465154);
        //Solve Part II
        assert_eq!(solve(recursive_required_fuel), 5194864);
    }
}